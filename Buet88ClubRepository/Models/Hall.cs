﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Buet88ClubRepository.Models
{
    public class Hall
    {
        [Key]
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
