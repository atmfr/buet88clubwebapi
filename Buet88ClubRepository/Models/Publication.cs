﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Buet88ClubRepository.Models
{
    public class Publication
    {
        [Key]
        public string Id { get; set; }

        public string Title { get; set; }
        public string FilePath { get; set; }
    }
}
