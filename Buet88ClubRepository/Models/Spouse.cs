﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Buet88ClubRepository.Models
{
    public class Spouse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string BloodGroup { get; set; }
        public string Occupation { get; set; }
        public string Designation { get; set; }
        public string Organization { get; set; }
        public string Hobby { get; set; }
        public string BirthDate { get; set; }
    }
}
