﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Buet88ClubRepository.Models
{
    public class Member
    {
        [Key]
        public string Code { get; set; }
        public string Name { get; set; }
        public string OtherName { get; set; }
        public string Picture { get; set; }
        public string MemberNumber { get; set; }
        public string Password { get; set; }
        public string RoomNumber { get; set; }
        public string Hall { get; set; }
        public string Department { get; set; }
        public string Occupation { get; set; }
        public string OfficeAddress { get; set; }
        public string Designation { get; set; }
        public string Organization { get; set; }
        public string RadioAddOrp { get; set; }
        public string OfficePhone { get; set; }
        public string ResidencePhone { get; set; }
        public string Fax { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Type { get; set; }
        public string Residence { get; set; }
        public string PermanentAddress { get; set; }
        public string MailingAddress { get; set; }
        public string BirthDate { get; set; }
        public string MarriageDate { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string BloodGroup { get; set; }
        public string Status { get; set; }
        public string Hobby { get; set; }
        public  string LogStatus { get; set; }
        public string ClubEx { get; set; }
        public string ClubDesignation { get; set; }
        public string ClubPosition { get; set; }
        public virtual ICollection<Child> Children { get; set; }
        public Spouse Spouse { get; set; }

    }
}
