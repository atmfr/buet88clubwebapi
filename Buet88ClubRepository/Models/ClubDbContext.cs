﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Buet88ClubRepository.Models
{
    public class ClubDbContext : DbContext

    {
        public ClubDbContext(DbContextOptions<ClubDbContext> options)
            : base(options)
        { }

        public DbSet<Hall> Halls { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Publication> Publications { get; set; }
        public DbSet<Child> Children { get; set; }
        // public DbSet<Spouse> Spouse { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
