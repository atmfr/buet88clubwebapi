﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Buet88ClubRepository.Models
{
    public class Child
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string BloodGroup { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Class { get; set; }
        public string Institute { get; set; }
        public string Hobby { get; set; }
        public string OtherInfo { get; set; }
    }
}
