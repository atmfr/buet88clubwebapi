﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Buet88ClubRepository.Models;

namespace Buet88ClubRepository.Repositories
{
    public interface ICrudRepository<TEntity> where TEntity: class
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(Object code);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(object code);


        int Save();

    }
}
