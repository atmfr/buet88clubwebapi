﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Buet88ClubRepository.Models;
using Microsoft.EntityFrameworkCore;

namespace Buet88ClubRepository.Repositories
{
    public class CrudRepository<TEntity> : ICrudRepository<TEntity> where TEntity: class
    {
        private  ClubDbContext _context;
        internal DbSet<TEntity> DbSet;
        public CrudRepository(ClubDbContext context)
        {
            _context = context;
            this.DbSet = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public TEntity Get(object code)
        {
            return DbSet.Find(code);
        }

        public void Insert(TEntity entity)
        {
            DbSet.Add(entity);
            Save();
        }

        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            Save();
        }

        public void Delete(object code)
        {
            var entity = Get(code);
            DbSet.Remove(entity);
            Save();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

    }
}
