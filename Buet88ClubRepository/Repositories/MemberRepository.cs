﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;
using Microsoft.EntityFrameworkCore;

namespace Buet88ClubRepository.Repositories
{
    public class MemberRepository : IMemberRepository
    {
        private  ClubDbContext _context;
        public MemberRepository(ClubDbContext context)
        {
            _context = context;
        }

        public void SetContext(ClubDbContext context)
        {
            _context = context;
        }
        public IQueryable<Member> GetAllMembers()
        {
            return _context.Members
                .Include(s => s.Spouse)
                .Include(c => c.Children);
        }

        public Member GetMember(string code)
        {
            return _context.Members
                    .Include(s => s.Spouse)
                    .Include(c => c.Children)
                .FirstOrDefault(m => m.Code == code);
        }

        public void InsertMember(Member member)
        {
            _context.Members.Add(member);
            Save();

        }

        public void DeleteMember(string code)
        {
            var member = GetMember(code);
            var children = member.Children.ToList();

            _context.Children.RemoveRange(children);
            _context.Members.Remove(member);
            Save();
        }

        public void UpdateMember(Member member)
        {
            _context.Members.Update(member);
            Save();
        }

        public int Save()
        {
           return  _context.SaveChanges();
        }
    }
}
