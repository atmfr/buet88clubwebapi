﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubRepository.Repositories
{
    public interface IMemberRepository
    {
        IQueryable<Member> GetAllMembers();
        Member GetMember(string code);
        void InsertMember(Member member);
        void UpdateMember(Member member);
        void DeleteMember(string code);
        int Save();
        void SetContext(ClubDbContext context);

    }
}
