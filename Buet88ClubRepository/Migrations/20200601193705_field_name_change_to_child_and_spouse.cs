﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class field_name_change_to_child_and_spouse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DoB",
                table: "Spouse");

            migrationBuilder.DropColumn(
                name: "DoB",
                table: "Children");

            migrationBuilder.AddColumn<string>(
                name: "BirthDate",
                table: "Spouse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BirthDate",
                table: "Children",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "Spouse");

            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "Children");

            migrationBuilder.AddColumn<DateTime>(
                name: "DoB",
                table: "Spouse",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DoB",
                table: "Children",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
