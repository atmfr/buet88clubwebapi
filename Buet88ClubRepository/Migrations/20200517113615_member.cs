﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class member : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Members_Departments_DepartmentCode",
                table: "Members");

            migrationBuilder.DropForeignKey(
                name: "FK_Members_Halls_HallCode",
                table: "Members");

            migrationBuilder.DropIndex(
                name: "IX_Members_DepartmentCode",
                table: "Members");

            migrationBuilder.DropIndex(
                name: "IX_Members_HallCode",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "DepartmentCode",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "HallCode",
                table: "Members");

            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Hall",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Hall",
                table: "Members");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentCode",
                table: "Members",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HallCode",
                table: "Members",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Members_DepartmentCode",
                table: "Members",
                column: "DepartmentCode");

            migrationBuilder.CreateIndex(
                name: "IX_Members_HallCode",
                table: "Members",
                column: "HallCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Departments_DepartmentCode",
                table: "Members",
                column: "DepartmentCode",
                principalTable: "Departments",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Halls_HallCode",
                table: "Members",
                column: "HallCode",
                principalTable: "Halls",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
