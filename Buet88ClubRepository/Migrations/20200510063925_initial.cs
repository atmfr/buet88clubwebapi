﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Departments",
            //    columns: table => new
            //    {
            //        Code = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Departments", x => x.Code);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Halls",
            //    columns: table => new
            //    {
            //        Code = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Halls", x => x.Code);
            //    });

            migrationBuilder.CreateTable(
                name: "Spouse",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    DoB = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spouse", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    Code = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OtherName = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    MemberNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    RoomNumber = table.Column<string>(nullable: true),
                    HallCode = table.Column<int>(nullable: true),
                    DepartmentCode = table.Column<int>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    OfficeAddress = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Residence = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    MailingAddress = table.Column<string>(nullable: true),
                    DoB = table.Column<DateTime>(nullable: false),
                    MarriageDate = table.Column<DateTime>(nullable: false),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    LogStatus = table.Column<string>(nullable: true),
                    ClubEx = table.Column<string>(nullable: true),
                    ClubDesignation = table.Column<string>(nullable: true),
                    ClubPosition = table.Column<string>(nullable: true),
                    SpouseId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.Code);
                    table.ForeignKey(
                        name: "FK_Members_Departments_DepartmentCode",
                        column: x => x.DepartmentCode,
                        principalTable: "Departments",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Members_Halls_HallCode",
                        column: x => x.HallCode,
                        principalTable: "Halls",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Members_Spouse_SpouseId",
                        column: x => x.SpouseId,
                        principalTable: "Spouse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Children",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    DoB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    Class = table.Column<string>(nullable: true),
                    Institute = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    MemberCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Children", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Children_Members_MemberCode",
                        column: x => x.MemberCode,
                        principalTable: "Members",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Children_MemberCode",
                table: "Children",
                column: "MemberCode");

            migrationBuilder.CreateIndex(
                name: "IX_Members_DepartmentCode",
                table: "Members",
                column: "DepartmentCode");

            migrationBuilder.CreateIndex(
                name: "IX_Members_HallCode",
                table: "Members",
                column: "HallCode");

            migrationBuilder.CreateIndex(
                name: "IX_Members_SpouseId",
                table: "Members",
                column: "SpouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Children");

            migrationBuilder.DropTable(
                name: "Members");

            //migrationBuilder.DropTable(
            //    name: "Departments");

            //migrationBuilder.DropTable(
            //    name: "Halls");

            migrationBuilder.DropTable(
                name: "Spouse");
        }
    }
}
