﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class memberupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OfficePhone",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RadioAddOrp",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResidencePhone",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fax",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "OfficePhone",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "RadioAddOrp",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "ResidencePhone",
                table: "Members");
        }
    }
}
