﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class member_spuse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SpouseId",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MemberCode",
                table: "Children",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Spouse",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    DoB = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spouse", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Members_SpouseId",
                table: "Members",
                column: "SpouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Children_MemberCode",
                table: "Children",
                column: "MemberCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Children_Members_MemberCode",
                table: "Children",
                column: "MemberCode",
                principalTable: "Members",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Members_Spouse_SpouseId",
                table: "Members",
                column: "SpouseId",
                principalTable: "Spouse",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Children_Members_MemberCode",
                table: "Children");

            migrationBuilder.DropForeignKey(
                name: "FK_Members_Spouse_SpouseId",
                table: "Members");

            migrationBuilder.DropTable(
                name: "Spouse");

            migrationBuilder.DropIndex(
                name: "IX_Members_SpouseId",
                table: "Members");

            migrationBuilder.DropIndex(
                name: "IX_Children_MemberCode",
                table: "Children");

            migrationBuilder.DropColumn(
                name: "SpouseId",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "MemberCode",
                table: "Children");
        }
    }
}
