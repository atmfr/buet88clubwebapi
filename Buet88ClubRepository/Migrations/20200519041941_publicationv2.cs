﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class publicationv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "File",
                table: "Publications");

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "Publications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "Publications");

            migrationBuilder.AddColumn<string>(
                name: "File",
                table: "Publications",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
