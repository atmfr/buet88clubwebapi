﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Buet88ClubRepository.Migrations
{
    public partial class member_changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Children",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    DoB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    Class = table.Column<string>(nullable: true),
                    Institute = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Children", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    Code = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OtherName = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    MemberNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    RoomNumber = table.Column<string>(nullable: true),
                    Hall = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    OfficeAddress = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    RadioAddOrp = table.Column<string>(nullable: true),
                    OfficePhone = table.Column<string>(nullable: true),
                    ResidencePhone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Residence = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    MailingAddress = table.Column<string>(nullable: true),
                    BirthDate = table.Column<string>(nullable: true),
                    MarriageDate = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Hobby = table.Column<string>(nullable: true),
                    LogStatus = table.Column<string>(nullable: true),
                    ClubEx = table.Column<string>(nullable: true),
                    ClubDesignation = table.Column<string>(nullable: true),
                    ClubPosition = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.Code);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Children");

            migrationBuilder.DropTable(
                name: "Members");
        }
    }
}
