﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Buet88ClubWebApi.Controllers
{
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private IDepartmentService _deptService;

        public DepartmentController(IDepartmentService deptService)
        {
            _deptService = deptService;
        }

        [HttpGet("club/departments")]
        public IActionResult GetAllDepartments()
        {
            try
            {
                var departments = _deptService.GetAllDepartments();
                return Ok(departments);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

        [HttpGet("club/departments/{code}")]
        public IActionResult GetDepartmentByCode(int code)
        {
            try
            {
                var department = _deptService.GetDepartment(code);
                return Ok(department);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

        [HttpPost("club/departments/create")]
        public IActionResult CreateDepartment([FromBody] Department department)
        {
            try
            {
                _deptService.InsertDepartment(department);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("club/departments/update/{code}")]
        public IActionResult UpdateDepartment(int code, [FromBody] Department department)
        {
            try
            {
                _deptService.UpdateDepartment(department, code);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("club/departments/delete/{code}")]
        public IActionResult DeleteDepartment(int code)
        {
            try
            {
                _deptService.DeleteDepartment(code);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

    }
}
