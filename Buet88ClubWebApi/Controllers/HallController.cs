﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Buet88ClubWebApi.Controllers
{
    [ApiController]
    public class HallController : ControllerBase
    {
        private IHallService _hallService;
        public HallController(IHallService hallService)
        {
            _hallService = hallService;
        }

       

        [HttpGet("club/halls")]
        public IActionResult GetAllHalls()
        {
            try
            {
                var halls = _hallService.GetAllHalls();
                return Ok(halls);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

        [HttpGet("club/halls/{code}")]
        public IActionResult GetSingleHall(int code)
        {
            
            try
            {
                var hall = _hallService.GetHall(code);
                return Ok(hall);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("club/halls/create")]
        public IActionResult CreateHall([FromBody] Hall hall)
        {
            try
            {
                _hallService.InsertHall(hall);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

        [HttpPut("club/halls/update/{code}")]
        public IActionResult UpdateHall(int code, [FromBody] Hall hall)
        {
            try
            {
                _hallService.UpdateHall(hall, code);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }

        [HttpDelete("club/halls/delete/{code}")]
        public IActionResult DeleteHall(int code)
        {
            try
            {
                _hallService.DeleteHall(code);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
            
        }
    }
}
