﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Buet88ClubWebApi.Controllers
{
    [ApiController]
    public class EventController : ControllerBase
    {
        private IEventService _eventService;
        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet("club/events")]
        public IActionResult GetEvents()
        {
            try
            {
                var events = _eventService.GetAllEvents();
                var eve = new Event();
                return Ok(events);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("club/events/{id}")]
        public IActionResult GetSingleEvent( string id )
        {
            try
            {
                var entity = _eventService.GetEvent(id);
                return Ok(entity);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPost("club/events/create")]
        public IActionResult InsertEvent([FromBody] Event eventData)
        {
            try
            {
                _eventService.InsertEvent(eventData);
                
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPut("club/events/update/{id}")]
        public IActionResult UpdateEvent(string id, [FromBody] Event eventData)
        {
            try
            {
                _eventService.UpdateEvent(eventData, id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpDelete("club/events/delete/{id}")]
        public IActionResult DeleteEvent(string id)
        {
            try
            {
                _eventService.DeleteEvent(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpGet("club/events/remove/image/{eventId}/{imageName}")]
        public IActionResult RemoveImage(string eventId, string imageName)
        {
            try
            {
                _eventService.RemoveImage(eventId, imageName);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }



    }
}
