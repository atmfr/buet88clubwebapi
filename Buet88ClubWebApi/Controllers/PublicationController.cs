﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Buet88ClubWebApi.Controllers
{
    
    [ApiController]
    public class PublicationController : ControllerBase
    {
        private IPublicationService _publicationService;
        public PublicationController(IPublicationService publicationService)
        {
            _publicationService = publicationService;
        }

        [HttpGet("club/publications")]
        public IActionResult GetPublications()
        {
            try
            {
                var publications = _publicationService.GetAllPublications();
                return Ok(publications);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("club/publications/{id}")]
        public IActionResult GetSinglePublication(string id)
        {
            try
            {
                var entity = _publicationService.GetPublication(id);
                return Ok(entity);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPost("club/publications/create")]
        public IActionResult InsertPublication([FromBody] Publication publication)
        {
            try
            {
                _publicationService.InsertPublication(publication);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPut("club/publications/update/{id}")]
        public IActionResult UpdatePublication(string id, [FromBody] Publication publication)
        {
            try
            {
                _publicationService.UpdatePublication(publication, id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpDelete("club/publications/delete/{id}")]
        public IActionResult DeletePublication(string id)
        {
            try
            {
                _publicationService.DeletePublication(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }

        }
    }
}