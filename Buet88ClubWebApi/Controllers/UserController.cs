﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Buet88ClubWebApi.Controllers
{

    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("club/login")]
        public IActionResult Login([FromBody] User user)
        {
            return Ok(_userService.Login(user));
            //try
            //{
            //    return Ok(_userService.Login(user));

            //}
            //catch (Exception e)
            //{
            //    return BadRequest();
            //}
        }

        [HttpGet("club/users")]
        public IActionResult GetAllUsers()
        {
            try
            {
                return Ok(_userService.GetAllUser());
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpPost("club/users/create")]
        public IActionResult CreateUser([FromBody] User user)
        {
            try
            {
                _userService.CreateUser(user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpPost("club/check-username-unique")]
        public IActionResult CheckUniqueUsername([FromBody] User userObject)
        {
            try
            {
                return Ok(_userService.IsUserNameUnique(userObject.UserName));
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpDelete("club/users/delete/{id}")]
        public IActionResult DeleteUser(string id)
        {
            try
            {
                _userService.DeleteUser(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        
    }
}