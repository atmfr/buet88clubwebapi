﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Buet88ClubWebApi.Controllers
{
    [ApiController]
    public class FileController : ControllerBase
    {
        
        public FileController()
        {
            
        }

        [HttpPost("club/upload/images"), DisableRequestSizeLimit]
        public IActionResult UploadImages()
        {
            try
            {
                var files = Request.Form.Files;
                if (files.Count == 0 || files.Any(f => f.Length == 0))
                {
                    return BadRequest();
                }
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var imagePaths = SaveFiles(files, folderName, pathToSave);

                

                return Ok(imagePaths);

            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpPost("club/upload/files"), DisableRequestSizeLimit]
        public IActionResult UploadFiles()
        {
            try
            {
                var files = Request.Form.Files;
                if (files.Count == 0 || files.Any(f => f.Length == 0))
                {
                    return BadRequest();
                }
                var folderName = Path.Combine("Resources", "Files");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var filePaths = SaveFiles(files, folderName, pathToSave);

                return Ok(filePaths);

            }
            catch (Exception e)
            {
                return BadRequest();
            }

        }

        private FileObject SaveFiles(IFormFileCollection files, string folderName, string pathToSave)
        {
            var commaSeparatedPaths = new List<string>();
            foreach (var file in files)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fileArray = fileName.Split(".");
                var ext = fileArray[^1];
                fileName = Guid.NewGuid().ToString() + "." + ext;

                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName); //you can add this path to a list and then return all dbPaths to the client if require
                commaSeparatedPaths.Add(dbPath);

                using var stream = new FileStream(fullPath, FileMode.Create);
                file.CopyTo(stream);
            }
            var paths =  string.Join(",", commaSeparatedPaths);

            var imagePathObject = new FileObject()
            {
                Paths = paths
            };

            return imagePathObject;
        }

        
    }
}