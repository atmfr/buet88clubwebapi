﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Buet88ClubRepository.Models;
using Buet88ClubService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Buet88ClubWebApi.Controllers
{
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IMemberService _memberService;
        public MemberController(IMemberService memberService)
        {
            _memberService = memberService;
        }

        [HttpGet("club/members")]
        public IActionResult GetAllMembers()
        {
            try
            {
                var entities = _memberService.GetAllMembers();
                return Ok(entities);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpGet("club/members/{code}")]
        public IActionResult GetMember(string code)
        {
            try
            {
                var member = _memberService.GetMember(code);
                return Ok(member);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpPost("club/members/create")]
        public IActionResult InsertMember([FromBody] Member member)
        {
            try
            {
                _memberService.InsertMember(member);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpPut("club/members/update/{code}")]
        public IActionResult UpdateMember(string code, [FromBody] Member member)
        {
            try
            {
                _memberService.UpdateMember(code, member);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }

        [HttpDelete("club/members/delete/{code}")]
        public IActionResult DeleteMember(string code)
        {
            try
            {
                _memberService.DeleteMember(code);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpDelete("club/children/delete/{id}")]
        public IActionResult DeleteChildren(string id)
        {
            try
            {
                _memberService.DeleteChild(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPut("club/members/change-picture/{code}")]
        public IActionResult ChangeProfilePic(string code, [FromBody]FileObject fObject)
        {
            try
            {
                _memberService.ChangeProfilePicture(code, fObject.Paths);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}
