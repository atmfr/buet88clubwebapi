﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Buet88ClubWebApi.Controllers
{

    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet("/")]
        public IActionResult Home()
        {
            return Ok("working");
        }

        [HttpGet("/countries")]
        public IActionResult GetCountryList()
        {
            List<string> CountryList = new List<string>();
            CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in CInfoList)
            {
                RegionInfo R = new RegionInfo(CInfo.LCID);
                if (!(CountryList.Contains(R.EnglishName)))
                {
                    CountryList.Add(R.EnglishName);
                }
            }

            CountryList.Sort();
            List<dynamic> CountryObjectList = new List<dynamic>();
            CountryList.ForEach(country =>
            {
                CountryObjectList.Add(new { name = country });
            });

            var countries = new {countries = CountryObjectList };
           
            return Ok(countries);
        }
    }
}