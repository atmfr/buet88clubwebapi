﻿using Buet88ClubRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Repositories;

namespace Buet88ClubService.Services
{
    public class UserService : IUserService
    {
        private readonly SecurityService _securityService;
        private readonly ICrudRepository<User> _userRepository;
        private readonly MemberRepository _memberRepository;

        public UserService(SecurityService securityService, ClubDbContext context)
        {
            _securityService = securityService;
            _userRepository = new CrudRepository<User>(context);
            _memberRepository = new MemberRepository(context);
        }
        public void CreateUser(User user)
        {
            // pass hashing
            var hashedPassword = _securityService.GetMd5Hash(user.Password);
            user.Password = hashedPassword;
            _userRepository.Insert(user);

        }

        public void DeleteUser(string id)
        {
            _userRepository.Delete(Guid.Parse(id));
        }

        // returns all user with having isMember No
        public IQueryable<User> GetAllUser()
        {
            return _userRepository.GetAll().Where(u => u.IsMember == "no");
        }



        public User GetUser(string id)
        {
            
            return _userRepository.Get(Guid.Parse(id));
        }

        public void UpdateUser(User user, string id)
        {
            throw new NotImplementedException();
        }

        public object Login(User user)
        {
            var userEntity = _userRepository.GetAll().Single(u => u.UserName == user.UserName);

            if (userEntity == null) return new { exist = false };
            
            var verified = _securityService.VerifyMd5Hash(user.Password, userEntity.Password);
            if (!verified) return new { exist = false };

            return new
            {
                exist = true,
                token = "JWT-Token",
                isAdmin = userEntity.IsAdmin == "yes",
                isMember = userEntity.IsMember == "yes",
                memberCode = GetMemberCode(userEntity)
            };
        }

        private string GetMemberCode(User userEntity)
        {
            if (userEntity.IsMember == "no") return null;

            var entity = _memberRepository.GetAllMembers()
                                .Single(m => m.Email == userEntity.UserName);

            return entity.Code;
        }

        public object IsUserNameUnique(string userName)
        {
            if (_userRepository.GetAll().Any(u => u.UserName == userName))
            {
                return new {isUnique = false};
            }

            return new {isUnique = true};
        }

        public IQueryable<User> GetAllUserWithMember()
        {
            return _userRepository.GetAll();
        }
    }
}
