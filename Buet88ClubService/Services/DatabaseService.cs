﻿using System;
using System.Collections.Generic;
using System.Text;
using Buet88ClubRepository.Models;
using Microsoft.EntityFrameworkCore;

namespace Buet88ClubService.Services
{
    public class DatabaseService
    {
        private static ClubDbContext _context;
        private DatabaseService(ClubDbContext context)
        {
            _context = context;
        }

        private static ClubDbContext context;
        public static ClubDbContext GetDbContext()
        {
            //if (context == null)
            //{
            //    context = new ClubDbContext();
            //}

            return _context;
        }
    }
}
