﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Buet88ClubService.Services
{
    public class SecurityService
    {
        private readonly MD5 _md5Hash;

        public SecurityService()
        {
            _md5Hash = CreateMD5();
        }

        private MD5 CreateMD5()
        {
           return MD5.Create();
        }

        public string GetMd5Hash(string input)
        {

            byte[] data = _md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public bool VerifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
