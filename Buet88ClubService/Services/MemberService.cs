﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Migrations;
using Buet88ClubRepository.Models;
using Buet88ClubRepository.Repositories;

namespace Buet88ClubService.Services
{
    public class MemberService : IMemberService
    {
        private readonly MemberRepository _memberRepository;
        private readonly ICrudRepository<Child> _childRepository;
        private readonly IUserService _userService;
        private ClubDbContext _context;
        
        public MemberService(ClubDbContext context, IUserService userService)
        {
            _context = context;
            _memberRepository = new MemberRepository(_context);
            _childRepository = new CrudRepository<Child>(context);
            _userService = userService;

        }
        public IQueryable<Member> GetAllMembers()
        {
            var members = _memberRepository.GetAllMembers();
            return members;
        }

        public Member GetMember(string code)
        {
            var member = _memberRepository.GetMember(code);
            return member;
        }

        public void InsertMember(Member member)
        {
            member.Code = Guid.NewGuid().ToString();
            member.Password = member.MemberNumber;
            _memberRepository.InsertMember(member);
        }

        public void UpdateMember(string code, Member member)
        {
            member.Code = code;
            _memberRepository.UpdateMember(member);
        }

        public void DeleteMember(string code)
        {
            //var userToDelete = _userService
            //    .GetAllUserWithMember()
            //    .Single(u => u.UserName == GetMember(code).Email);

            //_userService.DeleteUser(userToDelete.Id.ToString());
            _memberRepository.DeleteMember(code);
        }

        public void DeleteChild(string id)
        {
            
            _childRepository.Delete(Guid.Parse(id));
        }

        public void ChangeProfilePicture(string code, string path)
        {
            var member = _memberRepository.GetMember(code);
            member.Picture = path;
            _memberRepository.UpdateMember(member);
        }
    }
}
