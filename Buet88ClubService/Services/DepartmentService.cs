﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;
using Buet88ClubRepository.Repositories;

namespace Buet88ClubService.Services
{
    public class DepartmentService : IDepartmentService
    {

        private readonly ICrudRepository<Department> _departmentRepository;

        public DepartmentService(ClubDbContext context)
        {
            _departmentRepository = new CrudRepository<Department>(context);
            //_departmentRepository.SetContext(DatabaseService.GetDbContext());
        }
        public IQueryable<Department> GetAllDepartments()
        {
            return _departmentRepository.GetAll();
        }

        public Department GetDepartment(int code)
        {
            return _departmentRepository.Get(code);
        }

        public void InsertDepartment(Department department)
        {
            _departmentRepository.Insert(department);
        }

        public void UpdateDepartment(Department department, int code)
        {
            department.Code = code;
            _departmentRepository.Update(department);
        }

        public void DeleteDepartment(int code)
        {
            _departmentRepository.Delete(code);
        }
    }
}
