﻿using Buet88ClubRepository.Models;
using Buet88ClubRepository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buet88ClubService.Services
{
    public class PublicationService : IPublicationService
    {
        private readonly ICrudRepository<Publication> _publicationRepository;
        public PublicationService(ClubDbContext context)
        {
            _publicationRepository = new CrudRepository<Publication>(context);
        }
        public void DeletePublication(string id)
        {
            _publicationRepository.Delete(id);
        }

        public IQueryable<Publication> GetAllPublications()
        {
            return _publicationRepository.GetAll();
        }

        public Publication GetPublication(string id)
        {
            return _publicationRepository.Get(id);
        }

        public void InsertPublication(Publication publication)
        {
            publication.Id = Guid.NewGuid().ToString();
            _publicationRepository.Insert(publication);
        }

        public void UpdatePublication(Publication publication, string id)
        {
            publication.Id = id;
            _publicationRepository.Update(publication);
        }
    }
}
