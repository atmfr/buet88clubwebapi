﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IEventService
    {
        IQueryable<Event> GetAllEvents();
        Event GetEvent(string id);
        void InsertEvent(Event eventData);
        void UpdateEvent(Event eventData, string id);
        void DeleteEvent(string id);

        void RemoveImage(string id, string image);
    }
}
