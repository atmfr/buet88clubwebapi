﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IDepartmentService
    {
        IQueryable<Department> GetAllDepartments();
        Department GetDepartment(int code);
        void InsertDepartment(Department department);
        void UpdateDepartment(Department department, int code);
        void DeleteDepartment(int code);
    }
}
