﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;
using Buet88ClubRepository.Repositories;

namespace Buet88ClubService.Services
{
    public class EventService : IEventService
    {
        private readonly ICrudRepository<Event> _eventRepository;
        public EventService(ClubDbContext context)
        {
            _eventRepository = new CrudRepository<Event>(context);
        }

        public void DeleteEvent(string id)
        {
            _eventRepository.Delete(id);
        }

        public IQueryable<Event> GetAllEvents()
        {
            return _eventRepository.GetAll();
        }

        public Event GetEvent(string id)
        {
            return _eventRepository.Get(id);
        }

        public void InsertEvent(Event eventData)
        {
            eventData.Id = Guid.NewGuid().ToString();
            _eventRepository.Insert(eventData);
        }

        public void UpdateEvent(Event eventData, string id)
        {
            eventData.Id = id;
            _eventRepository.Update(eventData);
        }

        public void RemoveImage(string id, string image)
        {
            var eventData = GetEvent(id);
            var eventImages = eventData.Images;
            var imageList = new List<string>(eventImages.Split(','));

            if (imageList.Contains(image))
            {
                imageList.Remove(image);
            }

            eventData.Images = string.Join(",", imageList);

            UpdateEvent(eventData, id);
        }
    }
}
