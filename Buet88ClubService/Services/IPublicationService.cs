﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IPublicationService
    {
        IQueryable<Publication> GetAllPublications();
        Publication GetPublication(string id);
        void InsertPublication(Publication publication);
        void UpdatePublication(Publication publication, string id);
        void DeletePublication(string id);
    }
}
