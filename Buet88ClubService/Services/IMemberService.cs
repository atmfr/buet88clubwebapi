﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IMemberService
    {
        IQueryable<Member> GetAllMembers();
        Member GetMember(string code);
        void InsertMember(Member member);
        void UpdateMember(string code, Member member);
        void ChangeProfilePicture(string code, string path);
        void DeleteMember(string code);
        void DeleteChild(string id);



    }
}
