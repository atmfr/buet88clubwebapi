﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IHallService
    {
        IQueryable<Hall> GetAllHalls();
        Hall GetHall(int code);
        void InsertHall(Hall hall);
        void UpdateHall(Hall hall, int code);
        void DeleteHall(int code);

    }
}
