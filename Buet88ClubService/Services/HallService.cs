﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;
using Buet88ClubRepository.Repositories;

namespace Buet88ClubService.Services
{
    public class HallService : IHallService

    {
        private readonly ICrudRepository<Hall> _hallRepository;
        
        public HallService(ClubDbContext context)
        {
            _hallRepository = new CrudRepository<Hall>(context);
            //_hallRepository.SetContext(DatabaseService.GetDbContext());
        }

        public IQueryable<Hall> GetAllHalls()
        {
            return _hallRepository.GetAll();
        }

        public Hall GetHall(int code)
        {
            return _hallRepository.Get(code);
        }

        public void InsertHall(Hall hall)
        {
            var hallToInsert = new Hall
            {
                Name = hall.Name
            };
            _hallRepository.Insert(hallToInsert);
        }

        public void UpdateHall(Hall hall, int code)
        {
            hall.Code = code;
            _hallRepository.Update(hall);
        }

        public void DeleteHall(int code)
        {
            _hallRepository.Delete(code);
        }
    }
}
