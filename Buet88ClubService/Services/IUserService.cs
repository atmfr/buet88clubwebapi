﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buet88ClubRepository.Models;

namespace Buet88ClubService.Services
{
    public interface IUserService
    {
        IQueryable<User> GetAllUser();
        IQueryable<User> GetAllUserWithMember();
        User GetUser(string id);
        void CreateUser(User user);
        void UpdateUser(User user, string id);
        void DeleteUser(string id);
        object Login(User user);
        object IsUserNameUnique(string userName);
    }
}
